﻿#include "Checkpoint.h"

#include "MorphCharacter.h"
#include "Components/BoxComponent.h"

ACheckpoint::ACheckpoint()
{
	_boxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = _boxCollision;

	_boxCollision->OnComponentBeginOverlap.AddDynamic(this, &ACheckpoint::OnBeginOverlap);
}

int32 ACheckpoint::GetId() const
{
	return _checkpointId;
}

void ACheckpoint::OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
                                 UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool fromSweep,
                                 const FHitResult& sweepResult)
{
	if (AMorphCharacter* player = Cast<AMorphCharacter>(otherActor))
	{
		if (const ACheckpoint* lastCheckpoint = player->GetCheckpoint())
		{
			if (lastCheckpoint->GetId() < _checkpointId)
			{
				player->SetCheckpoint(this);
			}
		}
		else
		{
			player->SetCheckpoint(this);
		}

		SetActorEnableCollision(false);
	}
}
