﻿#pragma once

#include "GameFramework/Actor.h"
#include "Treasure.generated.h"

enum class EMorphingState;
class UNiagaraSystem;
class UNiagaraComponent;
class USphereComponent;

UCLASS()
class MORPH_API ATreasure : public AActor
{
	GENERATED_BODY()

public:
	ATreasure();

private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USphereComponent* _sphereCollision;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UNiagaraComponent* _niagaraComponent;

	UPROPERTY(EditAnywhere, Category = "Components")
	UStaticMeshComponent* _mesh;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UNiagaraSystem* _pickupNiagaraSystem;
	
	UPROPERTY(EditDefaultsOnly, Category = "Morph|Gameplay")
	EMorphingState _morphingStateRequired;

private:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor, UPrimitiveComponent* otherComp,
	                    int32 otherBodyIndex, bool fromSweep, const FHitResult& sweepResult);
};
