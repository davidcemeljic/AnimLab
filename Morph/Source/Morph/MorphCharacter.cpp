#include "MorphCharacter.h"

#include "Checkpoint.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"

AMorphCharacter::AMorphCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f); // ...at this rotation rate

	// Note: For faster iteration times these variables, and many more, can be tweaked in the Character Blueprint
	// instead of recompiling to adjust them
	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	_cameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	_cameraBoom->SetupAttachment(RootComponent);
	_cameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	_cameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	_followCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	_followCamera->SetupAttachment(_cameraBoom, USpringArmComponent::SocketName);
	// Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	_followCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	_targetMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TargetMesh"));
	_targetMesh->SetVisibility(false);
	_targetMesh->SetupAttachment(RootComponent);

	_morphNiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("MorphNiagaraComponent"));
	_morphNiagaraComponent->SetAutoActivate(false);
	_morphNiagaraComponent->SetupAttachment(GetMesh());

	_runningNiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("RunningNiagaraComponent"));
	_runningNiagaraComponent->SetupAttachment(GetMesh());

	_deathNiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("DeathNiagaraComponent"));
	_deathNiagaraComponent->SetupAttachment(GetMesh());
}

void AMorphCharacter::PostInitProperties()
{
	Super::PostInitProperties();

	if (_runningNiagaraComponent != nullptr && _morphConfigurations.Num() > 0)
	{
		_runningNiagaraComponent->SetAsset(_morphConfigurations[0].RunningNiagaraSystem);
	}
}

void AMorphCharacter::UnlockMorphingState(EMorphingState MorphingState)
{
	if (!_unlockedMorphingStates.Contains(MorphingState))
	{
		_unlockedMorphingStates.Add(MorphingState);
	}
}

EMorphingState AMorphCharacter::GetMorphingState() const
{
	return _morphingState;
}

ACheckpoint* AMorphCharacter::GetCheckpoint() const
{
	return _checkpoint.Get();
}

void AMorphCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (const APlayerController* playerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(playerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(_defaultMappingContext, 0);
		}
	}
}

void AMorphCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		//Jumping
		EnhancedInputComponent->BindAction(_jumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(_jumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		//Moving
		EnhancedInputComponent->BindAction(_moveAction, ETriggerEvent::Triggered, this, &AMorphCharacter::Move);

		//Looking
		EnhancedInputComponent->BindAction(_lookAction, ETriggerEvent::Triggered, this, &AMorphCharacter::Look);

		// Morphing
		EnhancedInputComponent->BindAction(_morphToHumanAction, ETriggerEvent::Triggered, this,
		                                   &AMorphCharacter::MorphToHuman);
		EnhancedInputComponent->BindAction(_morphToLionAction, ETriggerEvent::Triggered, this,
		                                   &AMorphCharacter::MorphToLion);
		EnhancedInputComponent->BindAction(_morphToCrowAction, ETriggerEvent::Triggered, this,
		                                   &AMorphCharacter::MorphToCrow);
	}
}

void AMorphCharacter::SetCheckpoint(const ACheckpoint* value)
{
	if (_checkpoint != value)
	{
		_checkpoint = value;
	}
}

void AMorphCharacter::KillPlayer()
{
	if (_isDead)
	{
		return;
	}

	_isDead = true;

	GetMesh()->SetVisibility(false);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	const uint8 morphingStateIndex = static_cast<uint8>(_morphingState);
	if (_morphConfigurations.IsValidIndex(morphingStateIndex))
	{
		_deathNiagaraComponent->SetAsset(_morphConfigurations[morphingStateIndex].KillNiagaraSystem);
		_deathNiagaraComponent->Activate();
	}

	if (AController* controller = GetController())
	{
		controller->SetIgnoreMoveInput(true);
	}

	GetWorldTimerManager().SetTimer(_respawnTimer, this, &AMorphCharacter::Respawn, _respawnDelay, false);
}

void AMorphCharacter::Respawn()
{
	GetMesh()->SetVisibility(true);
	EnableInput(Cast<APlayerController>(GetController()));
	_deathNiagaraComponent->Deactivate();

	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	if (_checkpoint.IsValid())
	{
		SetActorLocation(_checkpoint.Get()->GetActorLocation());
	}

	if (AController* controller = GetController())
	{
		controller->SetIgnoreMoveInput(false);
	}

	_isDead = false;
}

void AMorphCharacter::Move(const FInputActionValue& Value)
{
	const FVector MovementVector = Value.Get<FVector>();

	if (Controller != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		const FVector UpDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Z);

		// add movement 
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
		AddMovementInput(UpDirection, MovementVector.Z);
	}
}

void AMorphCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	const FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AMorphCharacter::MorphToHuman()
{
	if (_morphConfigurations.Num() > 0)
	{
		MorphTo(_morphConfigurations[0]);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No human morph configuration found"));
	}
}

void AMorphCharacter::MorphToLion()
{
	if (_morphConfigurations.Num() > 1)
	{
		MorphTo(_morphConfigurations[1]);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No lion morph configuration found"));
	}
}

void AMorphCharacter::MorphToCrow()
{
	if (_morphConfigurations.Num() > 2)
	{
		MorphTo(_morphConfigurations[2]);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No crow morph configuration found"));
	}
}

bool AMorphCharacter::CanMorphTo(const FMorphConfiguration& morphConfiguration) const
{
	return !_isMorphing && _morphingState != morphConfiguration.MorphingState && _unlockedMorphingStates.Contains(
		morphConfiguration.MorphingState);
}

void AMorphCharacter::MorphTo(const FMorphConfiguration& morphConfiguration)
{
	if (!CanMorphTo(morphConfiguration))
	{
		return;
	}

	_isMorphing = true;

	if (morphConfiguration.Mesh != nullptr)
	{
		_targetMesh->SetSkeletalMesh(morphConfiguration.Mesh);
	}

	_targetMesh->SetSkeletalMesh(morphConfiguration.Mesh);
	_targetMesh->SetAnimInstanceClass(morphConfiguration.AnimBP);

	_morphNiagaraComponent->SetAsset(morphConfiguration.MorphToNiagaraSystem);
	UNiagaraFunctionLibrary::OverrideSystemUserVariableSkeletalMeshComponent(
		_morphNiagaraComponent, "Target Skeletal Mesh", _targetMesh);
	_morphNiagaraComponent->ActivateSystem();

	GetMesh()->SetVisibility(false);

	TWeakObjectPtr<AMorphCharacter> weakThis = this;
	const FTimerDelegate timerDelegate = FTimerDelegate::CreateLambda([weakThis, &morphConfiguration]()
	{
		if (AMorphCharacter* morphCharacter = weakThis.Get())
		{
			morphCharacter->GetMesh()->SetSkeletalMesh(morphConfiguration.Mesh);
			morphCharacter->GetMesh()->SetAnimInstanceClass(morphConfiguration.AnimBP);
			morphCharacter->GetMesh()->SetVisibility(true);

			morphCharacter->_morphNiagaraComponent->Deactivate();
			morphCharacter->_runningNiagaraComponent->SetAsset(morphConfiguration.RunningNiagaraSystem);

			morphCharacter->GetCharacterMovement()->JumpZVelocity = morphConfiguration.JumpVelocity;
			morphCharacter->GetCharacterMovement()->MaxWalkSpeed = morphConfiguration.Speed;
			morphCharacter->GetCharacterMovement()->MaxFlySpeed = morphConfiguration.Speed;

			morphCharacter->_morphingState = morphConfiguration.MorphingState;

			morphCharacter->GetCharacterMovement()->SetMovementMode(morphConfiguration.MovementMode);

			morphCharacter->_isMorphing = false;
		}
	});

	GetWorldTimerManager().SetTimer(_morphingTimer, timerDelegate, 3.0f, false);
}
