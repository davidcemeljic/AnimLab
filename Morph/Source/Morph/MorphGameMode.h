// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MorphGameMode.generated.h"

UCLASS(minimalapi)
class AMorphGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMorphGameMode();
};



