﻿#include "Treasure.h"

#include "MorphCharacter.h"
#include "NiagaraComponent.h"
#include "Components/SphereComponent.h"
#include "Niagara/Public/NiagaraFunctionLibrary.h"

ATreasure::ATreasure()
{
	_sphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	RootComponent = _sphereCollision;

	_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	_mesh->SetupAttachment(RootComponent);

	_niagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraComponent"));
	_niagaraComponent->SetupAttachment(_mesh);

	_sphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ATreasure::OnBeginOverlap);
}

void ATreasure::OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
                               UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool fromSweep,
                               const FHitResult& sweepResult)
{
	if (const AMorphCharacter* player = Cast<AMorphCharacter>(otherActor))
	{
		if (player->GetMorphingState() == _morphingStateRequired)
		{
			if (_pickupNiagaraSystem != nullptr)
			{
				UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, _pickupNiagaraSystem,
				                                               _niagaraComponent->GetComponentLocation());
			}
			
			SetActorEnableCollision(false);
		}
	}
}
