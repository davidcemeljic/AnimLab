﻿#pragma once

#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

class UNiagaraSystem;
enum class EMorphingState;
class UNiagaraComponent;
class USkeletalMesh;
class USphereComponent;

UCLASS()
class MORPH_API APickup : public AActor
{
	GENERATED_BODY()

public:
	APickup();

private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USphereComponent* _sphereCollision;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UNiagaraComponent* _niagaraComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Morph|Particle Effects")
	USkeletalMeshComponent* _mesh;

	UPROPERTY(EditDefaultsOnly, Category = "Morph|Particle Effects")
	UNiagaraSystem* _pickupNiagaraSystem;

	UPROPERTY(EditDefaultsOnly, Category = "Morph|Gameplay")
	EMorphingState _morphingStateToUnlock;

	UPROPERTY(EditDefaultsOnly, Category = "Morph|Gameplay")
	EMorphingState _morphingStateRequired;

private:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor, UPrimitiveComponent* otherComp,
	                    int32 otherBodyIndex, bool fromSweep, const FHitResult& sweepResult);
	
	UFUNCTION()
	void OnPickupNiagaraSystemFinished(UNiagaraComponent* component);
};
