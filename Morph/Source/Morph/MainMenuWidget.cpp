﻿#include "MainMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void UMainMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (_playButton != nullptr)
	{
		_playButton->OnClicked.AddDynamic(this, &UMainMenuWidget::Play);
	}

	if (_quitButton != nullptr)
	{
		_quitButton->OnClicked.AddDynamic(this, &UMainMenuWidget::QuitGame);
	}
}

void UMainMenuWidget::Play()
{
	if (APlayerController* playerController = UGameplayStatics::GetPlayerController(this, 0))
	{
		playerController->bShowMouseCursor = false;
		playerController->SetInputMode(FInputModeGameOnly());
		
		RemoveFromParent();
	}

}

void UMainMenuWidget::QuitGame()
{
	FGenericPlatformMisc::RequestExit(false);
}
