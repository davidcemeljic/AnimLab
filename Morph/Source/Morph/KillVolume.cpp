﻿#include "KillVolume.h"

#include "MorphCharacter.h"
#include "Components/BoxComponent.h"

AKillVolume::AKillVolume()
{
	_boxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = _boxCollision;

	_boxCollision->OnComponentBeginOverlap.AddDynamic(this, &AKillVolume::OnBeginOverlap);
}

void AKillVolume::OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
                                 UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool fromSweep,
                                 const FHitResult& sweepResult)
{
	if (AMorphCharacter* player = Cast<AMorphCharacter>(otherActor))
	{
		if (_morphingStatesToKill.Contains(player->GetMorphingState()))
		{
			player->KillPlayer();
		}
	}
}
