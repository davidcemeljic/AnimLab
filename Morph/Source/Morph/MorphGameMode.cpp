// Copyright Epic Games, Inc. All Rights Reserved.

#include "MorphGameMode.h"
#include "MorphCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMorphGameMode::AMorphGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
