﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Checkpoint.generated.h"

class UBoxComponent;
UCLASS()
class MORPH_API ACheckpoint : public AActor
{
	GENERATED_BODY()

public:
	ACheckpoint();

	int32 GetId() const;
	
private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UBoxComponent* _boxCollision;

	UPROPERTY(EditAnywhere, Category = "Morph|Gameplay")
	int32 _checkpointId;

private:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor, UPrimitiveComponent* otherComp,
	                    int32 otherBodyIndex, bool fromSweep, const FHitResult& sweepResult);
};
