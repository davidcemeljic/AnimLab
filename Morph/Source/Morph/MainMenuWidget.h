﻿#pragma once

#include "Blueprint/UserWidget.h"
#include "MainMenuWidget.generated.h"

class UButton;

UCLASS()
class MORPH_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

private:
	UPROPERTY(meta = (BindWidget))
	UButton* _playButton;

	UPROPERTY(meta = (BindWidget))
	UButton* _quitButton;

private:
	UFUNCTION()
	void Play();

	UFUNCTION()
	void QuitGame();
};
