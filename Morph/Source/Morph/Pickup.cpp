﻿#include "Pickup.h"

#include "MorphCharacter.h"
#include "NiagaraComponent.h"
#include "Components/SphereComponent.h"

APickup::APickup()
{
	_sphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	RootComponent = _sphereCollision;

	_mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	_mesh->SetVisibility(false);
	_mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	_mesh->SetupAttachment(RootComponent);

	_niagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraComponent"));
	_niagaraComponent->SetupAttachment(_mesh);

	_sphereCollision->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnBeginOverlap);
}

void APickup::OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor,
                             UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool fromSweep,
                             const FHitResult& sweepResult)
{
	if (AMorphCharacter* player = Cast<AMorphCharacter>(otherActor))
	{
		if (player->GetMorphingState() == _morphingStateRequired)
		{
			player->UnlockMorphingState(_morphingStateToUnlock);

			_mesh->SetVisibility(false);

			_niagaraComponent->SetAsset(_pickupNiagaraSystem);
			_niagaraComponent->OnSystemFinished.AddUniqueDynamic(this, &APickup::OnPickupNiagaraSystemFinished);
		}
	}
}

void APickup::OnPickupNiagaraSystemFinished(UNiagaraComponent* component)
{
	Destroy();
}
