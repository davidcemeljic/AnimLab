#pragma once

#include "GameFramework/Character.h"
#include "MorphCharacter.generated.h"

class ACheckpoint;
struct FInputActionValue;
class UNiagaraSystem;
class UNiagaraComponent;
class UInputAction;
class UInputMappingContext;
class UCameraComponent;
class USpringArmComponent;

UENUM()
enum class EMorphingState { Human = 0, Lion, Crow };

USTRUCT()
struct FMorphConfiguration
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Morph")
	EMorphingState MorphingState;

	UPROPERTY(EditAnywhere, Category = "Morph")
	USkeletalMesh* Mesh;

	UPROPERTY(EditDefaultsOnly, Category = "Morphing")
	TSubclassOf<UAnimInstance> AnimBP;

	UPROPERTY(EditAnywhere, Category = "Morph")
	UNiagaraSystem* MorphToNiagaraSystem;

	UPROPERTY(EditAnywhere, Category = "Morph")
	UNiagaraSystem* RunningNiagaraSystem;

	UPROPERTY(EditAnywhere, Category = "Morph")
	UNiagaraSystem* KillNiagaraSystem;

	UPROPERTY(EditAnywhere, Category = "Morph")
	TEnumAsByte<EMovementMode> MovementMode;

	UPROPERTY(EditAnywhere, Category = "Morph")
	float Speed;

	UPROPERTY(EditAnywhere, Category = "Morph")
	float JumpVelocity;
};


UCLASS(Config = Game)
class AMorphCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMorphCharacter();

	virtual void PostInitProperties() override;

	void UnlockMorphingState(EMorphingState MorphingState);

	EMorphingState GetMorphingState() const;

	ACheckpoint* GetCheckpoint() const;

	void SetCheckpoint(const ACheckpoint* value);
	
	void KillPlayer();

	void Respawn();
	
protected:
	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);

	virtual void MorphToHuman();
	virtual void MorphToLion();
	virtual void MorphToCrow();

	// APawn 
protected:
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	virtual void BeginPlay() override;

private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USkeletalMeshComponent* _targetMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UNiagaraComponent* _morphNiagaraComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UNiagaraComponent* _runningNiagaraComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UNiagaraComponent* _deathNiagaraComponent;

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* _cameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* _followCamera;

	UPROPERTY(EditDefaultsOnly, Category = "Morphing")
	TArray<FMorphConfiguration> _morphConfigurations;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* _defaultMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* _jumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* _moveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* _lookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* _morphToHumanAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* _morphToLionAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* _morphToCrowAction;

	EMorphingState _morphingState;

	bool _isMorphing;

	FTimerHandle _morphingTimer;

	TArray<EMorphingState> _unlockedMorphingStates = {EMorphingState::Human};

	UPROPERTY(EditDefaultsOnly, Category = "Morphing")
	TSoftObjectPtr<ACheckpoint> _checkpoint;

	bool _isDead;
	
	FTimerHandle _respawnTimer;

	UPROPERTY(EditDefaultsOnly, Category = "Respawning")
	float _respawnDelay;

private:
	bool CanMorphTo(const FMorphConfiguration& morphConfiguration) const;
	void MorphTo(const FMorphConfiguration& morphConfiguration);
};
