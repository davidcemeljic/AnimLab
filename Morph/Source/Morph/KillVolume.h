﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "KillVolume.generated.h"

enum class EMorphingState;

class UBoxComponent;
UCLASS()
class MORPH_API AKillVolume : public AActor
{
	GENERATED_BODY()

public:
	AKillVolume();

private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UBoxComponent* _boxCollision;

	UPROPERTY(EditAnywhere, Category = "Morph|Gameplay")
	TArray<EMorphingState> _morphingStatesToKill;

private:
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* overlappedComponent, AActor* otherActor, UPrimitiveComponent* otherComp,
	                    int32 otherBodyIndex, bool fromSweep, const FHitResult& sweepResult);
};
