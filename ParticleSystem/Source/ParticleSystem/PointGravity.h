﻿#pragma once

#include "CoreMinimal.h"
#include "ParticleModifier.h"
#include "PointGravity.generated.h"

UCLASS()
class PARTICLESYSTEM_API UPointGravity : public UParticleModifier
{
	GENERATED_BODY()

public:
	virtual void ApplyToParticle(FParticleData& particle, float deltaTime) const override;

private:
	UPROPERTY(EditAnywhere)
	FVector _center;

	UPROPERTY(EditAnywhere)
	float _mass = 100.0f;

	UPROPERTY(EditAnywhere)
	float _maxForce = 100.0f;
};
