// Copyright Epic Games, Inc. All Rights Reserved.

#include "ParticleSystemGameMode.h"
#include "ParticleSystemCharacter.h"
#include "UObject/ConstructorHelpers.h"

AParticleSystemGameMode::AParticleSystemGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
