﻿#include "VortexForce.h"

void UVortexForce::ApplyToParticle(FParticleData& particle, float deltaTime) const
{
	const FVector centerToParticle = particle.transform.GetLocation() - _vortexCenter;
	const FVector vortexRandom = FMath::RandRange(-1.0f, 1.0f) * _axis * _vorticity;

	particle.force += (centerToParticle.RotateAngleAxis(_angle, _axis) - centerToParticle) * _strength /
		(centerToParticle.Length() * _distance) + vortexRandom;
}
