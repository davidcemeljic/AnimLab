﻿#pragma once

#include "CoreMinimal.h"
#include "SpawnLocation.generated.h"

UCLASS(Abstract, EditInlineNew)
class PARTICLESYSTEM_API USpawnLocation : public UDataAsset
{
	GENERATED_BODY()
	
public:
	virtual FVector GetRandomSpawnLocation() const PURE_VIRTUAL(USpawnLocation::GetRandomSpawnLocation, return FVector(););
};
