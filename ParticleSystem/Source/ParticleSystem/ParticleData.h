﻿#pragma once

#include "CoreMinimal.h"
#include "ParticleData.generated.h"

USTRUCT()
struct FParticleData
{
	GENERATED_BODY()

	FTransform transform = FTransform::Identity;
	FVector velocity = FVector::ZeroVector;
	FVector force = FVector::ZeroVector;
	
	float mass = 1.0f;

	FLinearColor color = FLinearColor::White;
	float lifeTime = 5.0f;
};
