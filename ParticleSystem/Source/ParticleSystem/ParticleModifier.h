﻿#pragma once

#include "CoreMinimal.h"
#include "ParticleData.h"
#include "ParticleModifier.generated.h"

UCLASS(Abstract, EditInlineNew)
class PARTICLESYSTEM_API UParticleModifier : public UDataAsset
{
	GENERATED_BODY()

public:
	virtual void ApplyToParticle(FParticleData& particle, float deltaTime) const PURE_VIRTUAL(UParticleModifier::ApplyToParticle, );
};
