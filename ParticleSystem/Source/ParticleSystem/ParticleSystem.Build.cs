// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ParticleSystem : ModuleRules
{
	public ParticleSystem(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
			{"Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "EnhancedInput", "GameplayTags"});
	}
}