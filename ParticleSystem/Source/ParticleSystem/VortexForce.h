﻿#pragma once

#include "CoreMinimal.h"
#include "ParticleModifier.h"
#include "VortexForce.generated.h"

UCLASS()
class PARTICLESYSTEM_API UVortexForce : public UParticleModifier
{
	GENERATED_BODY()
public:
	virtual void ApplyToParticle(FParticleData& particle, float deltaTime) const override;

private:
	UPROPERTY(EditAnywhere)
	FVector _vortexCenter = FVector(0.0f, 0.0f, 0.0f);

	UPROPERTY(EditAnywhere)
	FVector _axis = FVector(0.0f, 0.0f, 1.0f);

	UPROPERTY(EditAnywhere)
	float _angle = 45.0f;

	UPROPERTY(EditAnywhere)
	float _vorticity = 10000.0;
	
	UPROPERTY(EditAnywhere)
	float _strength = 100000.0;

	UPROPERTY(EditAnywhere)
	float _distance = 100.0f;
};
