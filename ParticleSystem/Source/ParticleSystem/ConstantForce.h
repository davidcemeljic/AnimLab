﻿#pragma once

#include "CoreMinimal.h"
#include "ParticleModifier.h"
#include "ConstantForce.generated.h"

UCLASS()
class PARTICLESYSTEM_API UConstantForce : public UParticleModifier
{
	GENERATED_BODY()
	
public:
	virtual void ApplyToParticle(FParticleData& particle, float deltaTime) const override;

private:
	UPROPERTY(EditAnywhere)
	FVector _force = FVector(0.0f, 0.0f, -9.81f);
};
