﻿#pragma once

#include "CoreMinimal.h"
#include "SpawnLocation.h"
#include "BoxSpawnLocation.generated.h"

UCLASS(Blueprintable)
class PARTICLESYSTEM_API UBoxSpawnLocation : public USpawnLocation
{
	GENERATED_BODY()
	
public:
	virtual FVector GetRandomSpawnLocation() const override;

private:
	UPROPERTY(EditAnywhere)
	FVector _min;

	UPROPERTY(EditAnywhere)
	FVector _max;

	FBox _spawnBox;
};
