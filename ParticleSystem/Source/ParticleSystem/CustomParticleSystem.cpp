﻿#include "CustomParticleSystem.h"

#include "ParticleData.h"
#include "SpawnLocation.h"
#include "Components/InstancedStaticMeshComponent.h"

ACustomParticleSystem::ACustomParticleSystem()
{
	PrimaryActorTick.bCanEverTick = true;

	_root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = _root;

	_instancedStaticMesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("InstancedStaticMesh"));
	_instancedStaticMesh->NumCustomDataFloats = 6;
	_instancedStaticMesh->SetupAttachment(_root);
}

void ACustomParticleSystem::PostInitProperties()
{
	Super::PostInitProperties();

	if (_instancedStaticMesh != nullptr)
	{
		UMaterialInstanceDynamic* material = UMaterialInstanceDynamic::Create(_particleConfiguration.material, this);
		material->SetScalarParameterValue("OrientToCamera", _orientToCamera ? 1.0f : 0.0f);

		_instancedStaticMesh->SetMaterial(0, material);
	}
}

void ACustomParticleSystem::BeginPlay()
{
	Super::BeginPlay();
}

void ACustomParticleSystem::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	_timeSinceLastSpawn += deltaTime;
	if (_timeSinceLastSpawn >= 1.0f / _spawnRate)
	{
		_timeSinceLastSpawn = 0.0f;
		SpawnParticle();
	}

	UpdateParticles(deltaTime);
}

void ACustomParticleSystem::SpawnParticle()
{
	FParticleData particleData;
	particleData.mass = _particleConfiguration.mass;
	particleData.color = _particleConfiguration.color;
	particleData.lifeTime = _particleConfiguration.lifetime;

	particleData.transform.SetLocation(_spawnLocationTag->GetRandomSpawnLocation());
	particleData.transform.SetScale3D(_particleConfiguration.scale);

	particleData.velocity = FMath::VRandCone(_particleConfiguration.initialVelocity.GetSafeNormal(),
	                                         FMath::DegreesToRadians(_particleConfiguration.velocityConeMaxAngle)) *
		_particleConfiguration.initialVelocity.Length();


	_particles.Add(particleData);

	const int32 index = _instancedStaticMesh->AddInstance(particleData.transform, false);
	_instancedStaticMesh->SetCustomData(index, {
		                                    particleData.color.R, particleData.color.G, particleData.color.B,
		                                    particleData.color.A, particleData.lifeTime, _particleConfiguration.lifetime
	                                    });
}

void ACustomParticleSystem::UpdateParticles(float deltaTime)
{
	for (int32 index = 0; index < _particles.Num(); ++index)
	{
		FParticleData& particleData = _particles[index];

		particleData.lifeTime -= deltaTime;
		if (particleData.lifeTime <= 0.0f)
		{
			DestroyParticle(index);
			--index;
			continue;
		}

		ApplyModifiersToParticle(index, deltaTime);

		const FVector acceleration = particleData.force / particleData.mass;
		particleData.velocity += acceleration * deltaTime;

		particleData.force = FVector::ZeroVector;

		particleData.transform.AddToTranslation(particleData.velocity * deltaTime);

		if (_orientToVelocity)
		{
			FVector dir = particleData.velocity.GetSafeNormal();
			FRotator rot = FRotationMatrix::MakeFromYZ(dir, FVector(0.0f, 0.0f, 1.0f)).Rotator();
			particleData.transform.SetRotation(rot.Quaternion());
		}

		_instancedStaticMesh->UpdateInstanceTransform(index, particleData.transform, false, false, true);

		_instancedStaticMesh->SetCustomData(index, {
			                                    particleData.color.R, particleData.color.G, particleData.color.B,
			                                    particleData.color.A, particleData.lifeTime,
			                                    _particleConfiguration.lifetime
		                                    });
	}

	_instancedStaticMesh->MarkRenderStateDirty();
}

void ACustomParticleSystem::DestroyParticle(int32 index)
{
	_particles.RemoveAt(index);
	_instancedStaticMesh->RemoveInstance(index);
}

void ACustomParticleSystem::ApplyModifiersToParticle(int32 index, float deltaTime)
{
	for (const UParticleModifier* particleModifier : _particleModifiers)
	{
		if (particleModifier != nullptr)
		{
			particleModifier->ApplyToParticle(_particles[index], deltaTime);
		}
	}
}
