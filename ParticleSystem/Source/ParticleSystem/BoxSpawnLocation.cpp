﻿#include "BoxSpawnLocation.h"

FVector UBoxSpawnLocation::GetRandomSpawnLocation() const
{
	return FVector(FMath::RandRange(_min.X, _max.X),
	               FMath::RandRange(_min.Y, _max.Y),
	               FMath::RandRange(_min.Z, _max.Z));
}
