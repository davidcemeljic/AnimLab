// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ParticleSystemGameMode.generated.h"

UCLASS(minimalapi)
class AParticleSystemGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AParticleSystemGameMode();
};



