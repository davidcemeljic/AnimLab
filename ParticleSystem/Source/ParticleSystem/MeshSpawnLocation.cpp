﻿#include "MeshSpawnLocation.h"

FVector UMeshSpawnLocation::GetRandomSpawnLocation() const
{
	if (_mesh == nullptr)
	{
		return FVector::ZeroVector;
	}

	if (_mesh->GetRenderData()->LODResources.Num() > 0)
	{
		if (const FPositionVertexBuffer* vertexBuffer = &_mesh->GetRenderData()->LODResources[0].VertexBuffers.
			PositionVertexBuffer)
		{
			const int32 vertexCount = vertexBuffer->GetNumVertices();
			if (vertexCount > 0)
			{
				const int32 randomVertexIndex = FMath::RandRange(0, vertexCount - 1);
				return FVector(vertexBuffer->VertexPosition(randomVertexIndex));
			}
		}
	}

	return Super::GetRandomSpawnLocation();
}
