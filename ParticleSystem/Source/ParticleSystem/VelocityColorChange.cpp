﻿#include "VelocityColorChange.h"

void UVelocityColorChange::ApplyToParticle(FParticleData& particle, float deltaTime) const
{
	particle.color = FLinearColor::LerpUsingHSV(_minColor, _maxColor, particle.velocity.Size() / _maxVelocity);
}
