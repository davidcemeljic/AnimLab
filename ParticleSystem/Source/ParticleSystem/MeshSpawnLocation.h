﻿#pragma once

#include "CoreMinimal.h"
#include "SpawnLocation.h"
#include "MeshSpawnLocation.generated.h"

UCLASS()
class PARTICLESYSTEM_API UMeshSpawnLocation : public USpawnLocation
{
	GENERATED_BODY()

public:
	virtual FVector GetRandomSpawnLocation() const override;

private:
	UPROPERTY(EditAnywhere)
	UStaticMesh* _mesh;
};
