﻿#include "SphereSpawnLocation.h"

FVector USphereSpawnLocation::GetRandomSpawnLocation() const
{
	FVector randomVector = FMath::VRand();
	randomVector.Normalize();
	randomVector *= FMath::FRandRange(0.0f, _radius);
	randomVector += _center;

	return randomVector;
}
