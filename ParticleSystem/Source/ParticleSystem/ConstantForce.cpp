﻿#include "ConstantForce.h"

void UConstantForce::ApplyToParticle(FParticleData& particle, float deltaTime) const
{
	particle.force += _force;
}
