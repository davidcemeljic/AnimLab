﻿#pragma once

#include "CoreMinimal.h"
#include "SpawnLocation.h"
#include "UObject/Object.h"
#include "SphereSpawnLocation.generated.h"

UCLASS()
class PARTICLESYSTEM_API USphereSpawnLocation : public USpawnLocation
{
	GENERATED_BODY()

public:
	virtual FVector GetRandomSpawnLocation() const override;

private:
	UPROPERTY(EditAnywhere)
	FVector _center;
	
	UPROPERTY(EditAnywhere)
	float _radius = 100.0f;
};
