﻿#pragma once

#include "CoreMinimal.h"
#include "ParticleModifier.h"
#include "VelocityColorChange.generated.h"

UCLASS()
class PARTICLESYSTEM_API UVelocityColorChange : public UParticleModifier
{
	GENERATED_BODY()

public:
	virtual void ApplyToParticle(FParticleData& particle, float deltaTime) const override;

private:
	UPROPERTY(EditAnywhere)
	float _maxVelocity = 1000.0f;

	UPROPERTY(EditAnywhere)
	FLinearColor _minColor = FLinearColor::Black;

	UPROPERTY(EditAnywhere)
	FLinearColor _maxColor = FLinearColor::White;
};
