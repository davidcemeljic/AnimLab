﻿#include "PointGravity.h"

void UPointGravity::ApplyToParticle(FParticleData& particle, float deltaTime) const
{
	const float distanceSquared = FMath::Max(1.0, FVector::DistSquared(particle.transform.GetLocation(), _center));

	const FVector direction = (_center - particle.transform.GetLocation()).GetSafeNormal();

	constexpr float G = 6.67408/*e-11f*/;

	particle.force += (direction * G * particle.mass * _mass / distanceSquared).GetClampedToSize(0.0f, _maxForce);
}
