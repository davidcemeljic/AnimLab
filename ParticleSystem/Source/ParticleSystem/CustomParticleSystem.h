﻿#pragma once

#include "CoreMinimal.h"
#include "ParticleModifier.h"
#include "GameFramework/Actor.h"
#include "CustomParticleSystem.generated.h"

struct FParticleData;
class UInstancedStaticMeshComponent;
class USpawnLocation;

USTRUCT()
struct FParticleConfiguration
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	float lifetime = 10.0f;

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	float mass = 1.0f;

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	UMaterialInterface* material;
	
	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	FLinearColor color = FLinearColor::White;

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	FVector scale = FVector::OneVector;

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	FVector initialVelocity = FVector(0.0f, 0.0f, 1.0f);

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	float velocityConeMaxAngle = 45.0f;
};

UCLASS()
class PARTICLESYSTEM_API ACustomParticleSystem : public AActor
{
	GENERATED_BODY()

public:
	ACustomParticleSystem();

	virtual void PostInitProperties() override;

	virtual void Tick(float deltaTime) override;

	[[nodiscard]] float GetSpawnRate() const
	{
		return _spawnRate;
	}

	void SetSpawnRate(float value)
	{
		_spawnRate = value;
	}
protected:
	virtual void BeginPlay() override;
	
private:
	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USceneComponent* _root;

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	UInstancedStaticMeshComponent* _instancedStaticMesh;

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	FParticleConfiguration _particleConfiguration;

	UPROPERTY(EditAnywhere, Instanced, Category = "Particle Configuration")
	USpawnLocation* _spawnLocationTag;

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	float _spawnRate = 10.0f;

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	bool _orientToCamera;

	UPROPERTY(EditAnywhere, Category = "Particle Configuration")
	bool _orientToVelocity; 

	UPROPERTY(EditAnywhere, Instanced, Category = "Particle Configuration")
	TArray<UParticleModifier*> _particleModifiers;

	TArray<FParticleData> _particles;

	float _timeSinceLastSpawn;


private:
	void SpawnParticle();
	void UpdateParticles(float deltaTime);
	void DestroyParticle(int32 index);

	void ApplyModifiersToParticle(int32 index, float deltaTime);
};
