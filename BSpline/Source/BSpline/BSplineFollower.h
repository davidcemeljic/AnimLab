﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BSplineFollower.generated.h"

class UArrowComponent;
class ABSplineActor;
class UStaticMeshComponent;
class USceneComponent;
class USphereComponent;

UCLASS()
class BSPLINE_API ABSplineFollower : public AActor {
	GENERATED_BODY()

public:
	ABSplineFollower();

	void StartFollowing();
	void StopFollowing();

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float deltaTime) override;

	void UpdateTransform(float deltaTime);


private:
	UPROPERTY(EditDefaultsOnly, Category = "BSpline")
	USceneComponent* _root;

	UPROPERTY(EditDefaultsOnly, Category = "BSpline")
	USphereComponent* _sphereCollision;

	UPROPERTY(EditDefaultsOnly, Category = "BSpline")
	UStaticMeshComponent* _mesh;

	UPROPERTY(EditDefaultsOnly, Category = "BSpline")
	UArrowComponent* _tangentArrow;

	
	UPROPERTY(EditInstanceOnly, Category = "BSpline")
	ABSplineActor* _spline;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	bool _looping = false;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	bool _useSplineNormal = true;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	float _progress = 0.0f;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	bool _isFollowing = false;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	float _speed = 1.0f;
};
