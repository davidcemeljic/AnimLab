// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BSplineGameMode.generated.h"

UCLASS(minimalapi)
class ABSplineGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABSplineGameMode();
};



