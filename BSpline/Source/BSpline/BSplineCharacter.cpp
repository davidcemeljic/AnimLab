// Copyright Epic Games, Inc. All Rights Reserved.

#include "BSplineCharacter.h"

#include "BSplineFollower.h"
#include "BSplineProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"


//////////////////////////////////////////////////////////////////////////
// ABSplineCharacter

ABSplineCharacter::ABSplineCharacter() {
	// Character doesnt have a rifle at start
	bHasRifle = false;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	//Mesh1P->SetRelativeRotation(FRotator(0.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-30.f, 0.f, -150.f));

}

void ABSplineCharacter::BeginPlay() {
	// Call the base class  
	Super::BeginPlay();

}

//////////////////////////////////////////////////////////////////////////// Input

void ABSplineCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) {
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		//Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		//Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ABSplineCharacter::Move);

		//Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ABSplineCharacter::Look);

		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this,
		                                   &ABSplineCharacter::Interact);
	}
}

void ABSplineCharacter::Interact() {
	TArray<AActor*> overlappingActors;
	GetCapsuleComponent()->GetOverlappingActors(overlappingActors, ABSplineFollower::StaticClass());

	if (_splineFollower != nullptr) return;

	for (AActor* actor : overlappingActors) {
		if (ABSplineFollower* follower = Cast<ABSplineFollower>(actor)) {
			AttachToActor(follower, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
			GetCharacterMovement()->GravityScale = 0.0f;
			
			SetActorRelativeLocation(FVector(00.0f, 0.0f, 150.0f));

			_splineFollower = follower;
			_splineFollower->StartFollowing();
			
			break;
		}
	}
}

void ABSplineCharacter::PossessedBy(AController* NewController) {
	Super::PossessedBy(NewController);

	//Add a mapping context on Possessed
	if (const APlayerController* PlayerController = Cast<APlayerController>(NewController)) {
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer())) {
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

void ABSplineCharacter::UnPossessed() {
	Super::UnPossessed();

	if (APlayerController* PlayerController = Cast<APlayerController>(GetController())) {
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer())) {
			Subsystem->RemoveMappingContext(DefaultMappingContext);
		}
	}
}

void ABSplineCharacter::Move(const FInputActionValue& Value) {
	if (_splineFollower != nullptr) return;

	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr) {
		// add movement 
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void ABSplineCharacter::Look(const FInputActionValue& Value) {
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr) {
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void ABSplineCharacter::Jump() {
	Super::Jump();
	if (_splineFollower != nullptr) {
		_splineFollower->StopFollowing();
		
		DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		GetCharacterMovement()->GravityScale = 1.0f;

		_splineFollower = nullptr;
	}
}

void ABSplineCharacter::SetHasRifle(bool bNewHasRifle) { bHasRifle = bNewHasRifle; }

bool ABSplineCharacter::GetHasRifle() { return bHasRifle; }
