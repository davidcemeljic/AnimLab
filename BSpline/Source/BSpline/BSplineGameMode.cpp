// Copyright Epic Games, Inc. All Rights Reserved.

#include "BSplineGameMode.h"
#include "BSplineCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABSplineGameMode::ABSplineGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
