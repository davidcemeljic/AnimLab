﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BSplineActor.h"

#include "MatrixTypes.h"
#include "ProceduralMeshComponent.h"
#include "Components/SphereComponent.h"

// Sets default values
ABSplineActor::ABSplineActor() {
	_proceduralMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ProceduralMesh"));
	SetRootComponent(_proceduralMesh);
}

void ABSplineActor::PostInitProperties() {
	Super::PostInitProperties();

	GenerateMesh();
}

void ABSplineActor::OnConstruction(const FTransform& Transform) {
	Super::OnConstruction(Transform);

	SpawnControlPointSpheres();
	GenerateSplinePoints();
	GenerateMesh();
}

void ABSplineActor::GenerateMesh() {
	if (_points.Num() <= 1) return;


	_internal_vertices.Reset();
	_internal_vertices.SetNum(_points.Num() * _ringSize);

	const FVector dir0to1 = (_points[1] - _points[0]).GetSafeNormal();
	const FVector zAxis{0, 0, 1};

	// todo when dir0to1 is parallel to zAxis
	//if (FVector::DotProduct(dir0to1, zAxis) < 0.0001f)
	const FVector right = FVector::CrossProduct(zAxis, dir0to1);

	_internal_vertices[0] = _points[0] + right * _thickness;
	for (int32 indexInRing = 1; indexInRing < _ringSize; ++indexInRing) {
		_internal_vertices[indexInRing] = _points[0] +
			right.RotateAngleAxis(360.0f / _ringSize * indexInRing, dir0to1) * _thickness;
	}

	// copy first ring, offset it and rotate it for smooth transition between lines
	for (int32 ringIndex = 1; ringIndex < _points.Num(); ++ringIndex) {
		const FVector point = _points[ringIndex];

		const FVector directionBefore = (point - _points[ringIndex - 1]).GetSafeNormal();
		const FVector directionAfter = ringIndex >= _points.Num() - 1
			                               ? directionBefore
			                               : (_points[ringIndex + 1] - point).GetSafeNormal();
		const FVector oldRingDirection = FVector::CrossProduct(
				_internal_vertices[(ringIndex - 1) * _ringSize + 1] - _internal_vertices[(ringIndex - 1) * _ringSize],
				_internal_vertices[(ringIndex - 1) * _ringSize + _ringSize - 1] - _internal_vertices[(ringIndex - 1) *
					_ringSize]).
			GetSafeNormal();

		// angle between direction before and after this point
		// const float rotationAngle =  (FMath::Acos(
		// 		FVector::DotProduct(directionBefore, directionAfter))
		// 	 / 2.0f /*+ FMath::Acos(FVector::DotProduct(oldRingDirection, directionBefore))*/);


		const FVector rotationAxis = _angleMultiplier *  FVector::CrossProduct(directionBefore, directionAfter);

		auto angleBetweenVectors = [](const FVector& a, const FVector& b, const FVector& axis) {
			return FMath::Atan2(
				FVector::CrossProduct(a, b).Dot(axis),
				FVector::DotProduct(a, b));
		};

		// const float rotationAngle = angleBetweenVectors(oldRingDirection, directionBefore, rotationAxis) +
		// 	angleBetweenVectors(directionBefore, directionAfter, rotationAxis) / 2.0f;

		const float rotationAngle = angleBetweenVectors(oldRingDirection, (directionBefore+directionAfter).GetSafeNormal(), rotationAxis);

		// take the average of the two directions meeting in this point
		const FVector offset = _points[ringIndex] - _points[ringIndex - 1]; // offset from previous point to this point

		const FVector ringAxis = (directionBefore + directionAfter) / 2.0f;

		FVector jointAxis = FVector::CrossProduct(directionBefore, directionAfter).
			GetSafeNormal();
		if (jointAxis.SizeSquared() < 0.0001f) jointAxis = FVector::CrossProduct(ringAxis, zAxis).GetSafeNormal();

		_internal_vertices[ringIndex * _ringSize] = point + jointAxis * _thickness;
		for (int32 indexInRing = 0; indexInRing < _ringSize; ++indexInRing) {
			// get location of the equivalent vertex in last ring, move it to the pivot by using its point's location,
			// rotate it around the pivot, move it back and then offset it
			FVector lastVertex = _internal_vertices[(ringIndex - 1) * _ringSize + indexInRing];
			lastVertex -= _points[ringIndex - 1];
			lastVertex = lastVertex.RotateAngleAxisRad(rotationAngle, rotationAxis);
			lastVertex += _points[ringIndex - 1] + offset;
			_internal_vertices[ringIndex * _ringSize + indexInRing] = lastVertex;
		}
	}

	// for each ring create _ringSize * 2 triangles
	_triangles.Reset();
	_triangles.Reserve((_points.Num() - 1) * _ringSize * 6);
	for (int32 ringIndex = 0; ringIndex < _points.Num() - 1; ++ringIndex) {
		for (int32 indexInRing = 0; indexInRing < _ringSize; ++indexInRing) {
			const int32 indexOffset = ringIndex * _ringSize;
			_triangles.Add(indexOffset + indexInRing);
			_triangles.Add(indexOffset + indexInRing + _ringSize);

			if (indexInRing == _ringSize - 1)
				_triangles.Add(indexOffset + indexInRing + 1);

			else _triangles.Add(indexOffset + indexInRing + _ringSize + 1);

			_triangles.Add(indexOffset + indexInRing);

			if (indexInRing == _ringSize - 1) _triangles.Add(indexOffset + indexInRing + 1);
			else _triangles.Add(indexOffset + indexInRing + _ringSize + 1);

			_triangles.Add(indexOffset + (indexInRing + 1) % _ringSize);
		}
	}
	_proceduralMesh->CreateMeshSection(0, _internal_vertices, _triangles, TArray<FVector>{}, TArray<FVector2D>{},
	                                   TArray<FColor>{}, TArray<FProcMeshTangent>{}, false);
}

void ABSplineActor::GenerateSplinePoints() {
	_points.Reset();
	_points.Reserve(_controlPoints.Num() * 2);

	const int32 numSegments = _controlPoints.Num() - 3;
	for (int32 segment = 1; segment <= numSegments; ++segment) {
		for (float t = 0.0f; t <= 1.0f; t += _resolution) _points.Add(GetSplinePoint(t, segment));
	}
}

void ABSplineActor::SpawnControlPointSpheres() {
	TArray<USphereComponent*> spheres;
	GetComponents<USphereComponent>(spheres);

	for (USphereComponent* sphere : spheres) sphere->DestroyComponent();

	for (const FVector controlPoint : _controlPoints) {
		USphereComponent* sphere = NewObject<USphereComponent>(this);
		sphere->SetSphereRadius(10.0f);
		sphere->RegisterComponent();
		sphere->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		sphere->SetRelativeLocation(controlPoint);
	}
}

FVector ABSplineActor::GetSplinePoint(float t, int32 segmentIndex) const {
	ensureMsgf(t >= 0.0f && t <= 1.0f, TEXT("ABSplineActor::GetSplinePoint: t must be between 0 and 1"));
	ensureMsgf(segmentIndex<= _controlPoints.Num() - 3,
	           TEXT("ABSplineActor::GetSplinePoint: segmentIndex must be less than the number of control points minus 3"
	           ));

	static const FMatrix B{
		{-1.0f, 3.0f, -3.0f, 1.0f},
		{3.0f, -6.0f, 3.0f, 0.0f},
		{-3.0f, 0.0f, 3.0f, 0.0f},
		{1.0f, 4.0f, 1.0f, 0.0f}
	};

	const FVector4 T{t * t * t, t * t, t, 1.0f};

	const FMatrix R{
		{_controlPoints[segmentIndex - 1], 0.0f},
		{_controlPoints[segmentIndex], 0.0f},
		{_controlPoints[segmentIndex + 1], 0.0f},
		{_controlPoints[segmentIndex + 2], 0.0f}
	};

	return Vector4MatrixMultiply(T, B * R) * 1.0f / 6.0f;
}

FVector ABSplineActor::GetSplinePoint(float t) const {
	ensureMsgf(t >= 0.0f && t <= 1.0f, TEXT("ABSplineActor::GetSplinePoint: t must be between 0 and 1"));

	const int32 numSegments = _controlPoints.Num() - 3;
	const int32 segment = FMath::FloorToInt(t * numSegments);
	t = t * numSegments - segment;

	return GetSplinePoint(t, segment + 1);
}

FVector ABSplineActor::GetSplineTangent(float t) const {
	ensureMsgf(t >= 0.0f && t <= 1.0f, TEXT("ABSplineActor::GetSplineTangent: t must be between 0 and 1"));

	const int32 numSegments = _controlPoints.Num() - 3;
	const int32 segment = FMath::FloorToInt(t * numSegments);
	t = t * numSegments - segment;

	return GetSplineTangent(t, segment + 1);
}

FVector ABSplineActor::GetSplineNormal(float t) const {
	ensureMsgf(t >= 0.0f && t <= 1.0f, TEXT("ABSplineActor::GetSplineNormal: t must be between 0 and 1"));

	const int32 numSegments = _controlPoints.Num() - 3;
	const int32 segment = FMath::FloorToInt(t * numSegments);
	t = t * numSegments - segment;

	return GetSplineNormal(t, segment + 1);
}

FVector ABSplineActor::GetSplineTangent(float t, int32 segmentIndex) const {
	ensureMsgf(t >= 0.0f && t <= 1.0f, TEXT("ABSplineActor::GetSplineTangent: t must be between 0 and 1"));
	ensureMsgf(segmentIndex<= _controlPoints.Num() - 3,
	           TEXT(
		           "ABSplineActor::GetSplineTangent: segmentIndex must be less than the number of control points minus 3"
	           ));

	const FVector4 dT{t * t, t, 1.0f, 0.0f};

	static const FMatrix B{
		{-1.0f, 3.0f, -3.0f, 1.0f},
		{2.0f, -4.0f, 2.0f, 0.0f},
		{-1.0f, 0.0f, 1.0f, 0.0f},
		{0.0f, 0.0f, 0.0f, 0.0f}
	};

	const FMatrix R{
		{_controlPoints[segmentIndex - 1], 0.0f},
		{_controlPoints[segmentIndex], 0.0f},
		{_controlPoints[segmentIndex + 1], 0.0f},
		{_controlPoints[segmentIndex + 2], 0.0f}
	};

	return Vector4MatrixMultiply(dT, B * R) * 1.0f / 2.0f;
}

FVector ABSplineActor::GetSplineNormal(float t, int32 segmentIndex) const {
	ensureMsgf(t >= 0.0f && t <= 1.0f, TEXT("ABSplineActor::GetSplineNormal: t must be between 0 and 1"));
	ensureMsgf(segmentIndex<= _controlPoints.Num() - 3,
	           TEXT(
		           "ABSplineActor::GetSplineNormal: segmentIndex must be less than the number of control points minus 3"
	           ));

	const FVector4 ddT{6 * t, 2, 0.0f, 0.0f};

	static const FMatrix B{
		{-1.0f, 3.0f, -3.0f, 1.0f},
		{2.0f, -4.0f, 2.0f, 0.0f},
		{-1.0f, 0.0f, 1.0f, 0.0f},
		{0.0f, 0.0f, 0.0f, 0.0f}
	};

	const FMatrix R{
		{_controlPoints[segmentIndex - 1], 0.0f},
		{_controlPoints[segmentIndex], 0.0f},
		{_controlPoints[segmentIndex + 1], 0.0f},
		{_controlPoints[segmentIndex + 2], 0.0f}
	};

	const FVector ddP = Vector4MatrixMultiply(ddT, B * R) * 1.0f / 6.0f;
	const FVector tangent = GetSplineTangent(t, segmentIndex);

	return FVector::CrossProduct(tangent, ddP).GetSafeNormal();
}

FVector ABSplineActor::Vector4MatrixMultiply(const FVector4& vector, const FMatrix& matrix) {
	FVector4 result;
	for (int i = 0; i < 4; ++i) {
		result[i] = 0.0f;
		for (int j = 0; j < 4; ++j) { result[i] += matrix.M[j][i] * vector[j]; }
	}
	return result;
}
