﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BSplineActor.generated.h"

class UProceduralMeshComponent;

UCLASS()
class BSPLINE_API ABSplineActor : public AActor {
	GENERATED_BODY()

public:
	ABSplineActor();

	virtual void PostInitProperties() override;
	virtual void OnConstruction(const FTransform& Transform) override;

	FVector GetSplinePoint(float t) const;
	FVector GetSplineTangent(float t) const;
	FVector GetSplineNormal(float t) const;

protected:
	UFUNCTION(BlueprintCallable, Category = "BSpline")
	virtual void GenerateMesh();

	void GenerateSplinePoints();
	void SpawnControlPointSpheres();

	FVector GetSplinePoint(float t, int32 segmentIndex) const;
	FVector GetSplineTangent(float t, int32 segmentIndex) const;
	FVector GetSplineNormal(float t, int32 segmentIndex) const;

private:
	UPROPERTY(EditDefaultsOnly, Category = "BSpline")
	UProceduralMeshComponent* _proceduralMesh;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	TArray<FVector> _controlPoints;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	float _thickness = 10.0f;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	int32 _ringSize = 8;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	float _resolution = 0.099f;

	UPROPERTY(EditAnywhere, Category = "BSpline")
	float _angleMultiplier = 6.0f;

	TArray<FVector> _points;
	TArray<FVector> _internal_vertices;
	TArray<int32> _triangles;

private:
	static FVector Vector4MatrixMultiply(const FVector4& vector, const FMatrix& matrix);
};
