// Copyright Epic Games, Inc. All Rights Reserved.

#include "BSpline.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BSpline, "BSpline" );
 