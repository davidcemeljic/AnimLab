﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BSplineFollower.h"

#include "BSplineActor.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"

ABSplineFollower::ABSplineFollower() {
	PrimaryActorTick.bCanEverTick = true;

	_root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(_root);

	_sphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	_sphereCollision->SetupAttachment(_root);

	_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	_mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	_mesh->SetupAttachment(_root);

	_tangentArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("TangentArrow"));
	_tangentArrow->SetHiddenInGame(false);
	_tangentArrow->SetupAttachment(_root);
}

void ABSplineFollower::StartFollowing() {
	if (_spline == nullptr) return;

	_isFollowing = true;
}

void ABSplineFollower::StopFollowing() { _isFollowing = false; }

void ABSplineFollower::BeginPlay() {
	Super::BeginPlay();

	if (_spline != nullptr) {
		AttachToActor(_spline, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		UpdateTransform(_progress);
	}
}

void ABSplineFollower::Tick(float deltaTime) {
	Super::Tick(deltaTime);

	if (_isFollowing)
		UpdateTransform(deltaTime);
}

void ABSplineFollower::UpdateTransform(float deltaTime) {
	if (_spline != nullptr) {
		_progress += deltaTime * _speed;
		if (_progress > 1.0f) {
			_progress = 0.0f;
			if (!_looping) {
				_isFollowing = false;
				return;
			}
		}

		const FVector location = _spline->GetSplinePoint(_progress);
		SetActorRelativeLocation(location);

		const FVector tangent = _spline->GetSplineTangent(_progress);

		if (_useSplineNormal) {
			const FVector normal = _spline->GetSplineNormal(_progress);
			const FRotator rotation = FRotationMatrix::MakeFromXZ(tangent, normal).Rotator();
			SetActorRelativeRotation(rotation);
		}
		else {
			const FRotator rotation = FRotationMatrix::MakeFromX(tangent).Rotator();
			SetActorRelativeRotation(rotation);
		}
	}
}
